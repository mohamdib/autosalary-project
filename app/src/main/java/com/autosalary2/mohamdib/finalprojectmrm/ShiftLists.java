package com.autosalary2.mohamdib.finalprojectmrm;

import android.content.ContentUris;
import android.content.ContentValues;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.ActivityInfo;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.net.Uri;
import android.provider.CalendarContract;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;


// this is the shifts activity , so the user can choose any month , and see all the shifts in this month
// and he can share the list also  by Share Button


public class ShiftLists extends AppCompatActivity {

    ListView listOfShifts;
    ShiftsDbHelper dbHelper;
    ShiftsCursorAdapter sca;
    Button btBack,btShare,btRemoveAll,btAddDay,btCalendar;
    String timeIn,timeOut;
    TextView tvTotal; // total salary for current month
    Spinner spMonth; // spinner to choose month
    String textToShare; // string that will contain the shifts to share it
    String clockIn,clockOut;
    String currentMonth; // current month
    private long ItemId;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        this.getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        getSupportActionBar().hide();
        setContentView(R.layout.activity_shift_lists);
        setRequestedOrientation (ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
        btCalendar= (Button) findViewById(R.id.btCalendar);
        listOfShifts=(ListView)findViewById(R.id.shiftsListVew);
        tvTotal=(TextView)findViewById(R.id.tvTotalSalary);
        btShare=(Button)findViewById(R.id.btShare);
        btBack=(Button)findViewById(R.id.btHome);
        btRemoveAll = (Button) findViewById(R.id.btRemoveAll);
        btAddDay = (Button) findViewById(R.id.btAddDay);
        dbHelper=new ShiftsDbHelper(this);
        SQLiteDatabase db = dbHelper.getReadableDatabase();
        timeIn="";
        timeOut="";
        textToShare="";
        currentMonth="";
        btCalendar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Uri.Builder builder = CalendarContract.CONTENT_URI.buildUpon();
                builder.appendPath("time");
                ContentUris.appendId(builder, Calendar.getInstance().getTimeInMillis());
                Intent intent = new Intent(Intent.ACTION_VIEW)
                        .setData(builder.build());
                startActivity(intent);
            }
        });
        // share button , using intent with ACTION_SEND flag ..
        btAddDay.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                View view= LayoutInflater.from(ShiftLists.this).inflate(R.layout.activity_add_day,null);
                final EditText edHoursIn = (EditText) view.findViewById(R.id.edHoursIn);
                final EditText edHoursOut = (EditText) view.findViewById(R.id.edHoursOut);
                final EditText edMinsIn = (EditText) view.findViewById(R.id.edMinIn);
                final EditText edMinsOut = (EditText) view.findViewById(R.id.edMinOut);


                edHoursIn.requestFocus();
                InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
                imm.toggleSoftInput(InputMethodManager.SHOW_FORCED, InputMethodManager.HIDE_IMPLICIT_ONLY);
                edHoursIn.setNextFocusDownId(R.id.edMinIn);
                edMinsIn.setNextFocusDownId(R.id.edHoursOut);
                edHoursOut.setNextFocusDownId(R.id.edMinOut);
                setFocuse(edHoursIn);
                setFocuse(edHoursOut);
                setFocuse(edMinsIn);
                setFocuse(edMinsOut);
                edHoursIn.setSelectAllOnFocus(true);
                edHoursOut.setSelectAllOnFocus(true);
                edMinsIn.setSelectAllOnFocus(true);
                edMinsOut.setSelectAllOnFocus(true);



                final TextView tvAddDay = (TextView)view.findViewById(R.id.tvAddDay);


                tvAddDay.setText(tvAddDay.getText().toString()+" "+getDate());
                AlertDialog.Builder builder = new AlertDialog.Builder(ShiftLists.this);
                builder.setMessage("הוספת יום באופן ידני:");
                builder.setView(view);
                builder.setPositiveButton("אישור", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        clockIn =edHoursIn.getText().toString()+":"+edMinsIn.getText().toString();
                        clockOut=edHoursOut.getText().toString()+":"+edMinsOut.getText().toString();
                        if (edHoursIn.getText().toString().equals("")||
                                edHoursOut.getText().toString().equals("")||
                                edMinsIn.getText().toString().equals("")||
                                edMinsOut.getText().toString().equals(""))

                        {

                            printToast("הוספה לא חוקית!");

                        }
                        else {


                            boolean isInValid=checkValidation(clockIn);
                            boolean isOutValid=checkValidation(clockOut);
                            if(!isInValid||!isOutValid)
                            {
                                printToast("שעות לא חוקיות! אנא נסה שנית");
                            }
                            else {
                                Calendar c = Calendar.getInstance();
                                int m = c.get(Calendar.MONTH);
                                m++;
                               // printToast("edin:" + edIn.getText().toString() + ",edOut:" + edOut.getText().toString());
                                SQLiteDatabase db = dbHelper.getReadableDatabase();
                                db = dbHelper.getWritableDatabase();
                                ContentValues values = new ContentValues();
                                values.put(Shift.DATE, getDate());
                                values.put(Shift.SHIFT_IN, clockIn);
                                values.put(Shift.SHIFT_OUT, clockOut);
                                timeIn = clockIn;
                                timeOut = clockOut;
                                values.put(Shift.TOTAL_SHIFTS, getTotalTime().toString());
                                values.put(Shift.MONTH, "" + m);
                                long id2;
                                id2 = db.insert(Shift.TABLE_NAME, null, values);
                                db.close();

                                db = dbHelper.getReadableDatabase();
                                Cursor cursor = db.query(Shift.TABLE_NAME, null, null, null, null, null, null);
                                sca = new ShiftsCursorAdapter(ShiftLists.this, cursor);
                                listOfShifts.setAdapter(sca);
                                db.close();
                            }
                        }


                        getWindow().setSoftInputMode(
                                WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN
                        );
                    }
                });

                builder.setNegativeButton("בטל", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {

                    }
                });
                AlertDialog dialog = builder.create();
                dialog.show();
            }
        });
        btShare.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                updateShiftsList(getNumOfMonth(currentMonth));
                if(!textToShare.equals("")) {
                    Intent sharingIntent = new Intent(android.content.Intent.ACTION_SEND);
                    sharingIntent.setType("text/plain");
                    sharingIntent.putExtra(android.content.Intent.EXTRA_SUBJECT, "משמרות לחודש:" + currentMonth);
                    sharingIntent.putExtra(android.content.Intent.EXTRA_TEXT, textToShare);
                    startActivity(Intent.createChooser(sharingIntent, "שתף את המשמרות"));
                }
                else
                {
                    printToast("אין משמרות בחודש זה! ");
                }
            }
        });

        //
        Cursor cursor = db.query(Shift.TABLE_NAME, null, null, null, null, null, null);
        sca=new ShiftsCursorAdapter(ShiftLists.this,cursor);
        listOfShifts.setAdapter(sca);
        db.close();
        listOfShifts.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

                printToast("נא ללחוץ לחיצה ארוכה כדי למחוק משמרת");
            }
        });
        btRemoveAll.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                AlertDialog.Builder builder = new AlertDialog.Builder(ShiftLists.this);
                builder.setTitle("האם ברצונך למחוק את כל המשמרות לחודש זה ? ");
                builder.setPositiveButton("כן", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        removeAll();
                        dialog.dismiss();
                    }
                });
                builder.setNegativeButton("לא", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        dialog.dismiss();
                    }
                });
                AlertDialog dialog = builder.create();
                dialog.show();
            }
        });
        listOfShifts.setOnItemLongClickListener(new AdapterView.OnItemLongClickListener() {
            @Override
            public boolean onItemLongClick(AdapterView<?> parent, View view, int position, long id) {

                ItemId=id;
                AlertDialog.Builder builder = new AlertDialog.Builder(ShiftLists.this);
                builder.setTitle("האם ברצונך למחוק משמרת זו? ");
                builder.setPositiveButton("כן", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        deleteItem(ItemId);
                        dialog.dismiss();
                    }
                });
                builder.setNegativeButton("לא", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        dialog.dismiss();
                    }
                });
                AlertDialog dialog = builder.create();
                dialog.show();

                

                return false;
            }
        });
        btBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i=new Intent(ShiftLists.this,MainActivity.class);
                startActivity(i);
            }
        });
        spMonth = (Spinner)findViewById(R.id.spMonth);
        // set the default list to be the current month
        Calendar c = Calendar.getInstance();
        int m = c.get(Calendar.MONTH);
        initSpinner();
        spMonth.setSelection(m);
        updateShiftsList(m);
        spinnerListener();



    }

    private void setFocuse(final EditText ed) {
        ed.addTextChangedListener(new TextWatcher() {

            public void onTextChanged(CharSequence s, int start,int before, int count)
            {
                // TODO Auto-generated method stub
                if(ed.getText().toString().length()==2)     //size as per your requirement
                {
                    ed.requestFocus();
                }
            }
            public void beforeTextChanged(CharSequence s, int start,
                                          int count, int after) {
                // TODO Auto-generated method stub

            }

            public void afterTextChanged(Editable s) {
                // TODO Auto-generated method stub
            }

        });

    }

    private boolean checkValidation(String s) {
        String[] clock=s.split(":");
        if(clock.length==1)
            return false;
        int hour=Integer.parseInt(clock[0].toString());
        int minute=Integer.parseInt(clock[1].toString());
        if(hour>23||hour<0||minute>59||minute<0)
            return false;
        return true;

    }

    // will delete all of the shifts according to the month
    private void removeAll() {
        if(textToShare.equals("")) {
            printToast("אין משמרות לחודש זה !");
            return;
        }
        int month=getNumOfMonth(currentMonth);
        SQLiteDatabase db = dbHelper.getReadableDatabase();
        Cursor cursor = db.query(Shift.TABLE_NAME, null, null, null, null, null, null);
        do {
            db.delete(Shift.TABLE_NAME,"month = ?",new String[]{""+month});

        } while (cursor.moveToNext());
        sca=new ShiftsCursorAdapter(ShiftLists.this,cursor);
        listOfShifts.setAdapter(sca);
        db.close();
        textToShare="";


    }

    private void deleteItem(long id) {
        SQLiteDatabase db = dbHelper.getReadableDatabase();
        Cursor cursor = db.query(Shift.TABLE_NAME, null, null, null, null, null, null);
        db.delete(Shift.TABLE_NAME,"_id=?",new String[]{""+id});
        sca=new ShiftsCursorAdapter(ShiftLists.this,cursor);
        listOfShifts.setAdapter(sca);
        db.close();
    }

    // spinner values (months in words)
    private void spinnerListener() {
        spMonth.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {

                if(parent.getItemAtPosition(position).toString().equals("ינואר"))
                {
                    currentMonth="ינואר";
                    updateShiftsList(1);
                }
                if(parent.getItemAtPosition(position).toString().equals("פברואר"))
                {
                    currentMonth="פברואר";
                    updateShiftsList(2);
                }
                if(parent.getItemAtPosition(position).toString().equals("מרץ"))
                {
                    currentMonth="מרץ";
                    updateShiftsList(3);
                }
                if(parent.getItemAtPosition(position).toString().equals("אפריל"))
                {
                    currentMonth="אפריל";
                    updateShiftsList(4);
                }
                if(parent.getItemAtPosition(position).toString().equals("מאי"))
                {
                    currentMonth="מאי";
                    updateShiftsList(5);
                }
                if(parent.getItemAtPosition(position).toString().equals("יוני"))
                {
                    currentMonth="יוני";
                    updateShiftsList(6);
                }
                if(parent.getItemAtPosition(position).toString().equals("יולי"))
                {
                    currentMonth="יולי";
                    updateShiftsList(7);
                }
                if(parent.getItemAtPosition(position).toString().equals("אוגוסט"))
                {
                    currentMonth="אוגוסט";
                    updateShiftsList(8);
                }
                if(parent.getItemAtPosition(position).toString().equals("ספטמבר"))
                {
                    currentMonth="ספטמבר";
                    updateShiftsList(9);
                }
                if(parent.getItemAtPosition(position).toString().equals("אוקטובר"))
                {
                    currentMonth="אוקטובר";
                    updateShiftsList(10);
                }
                if(parent.getItemAtPosition(position).toString().equals("נובמבר"))
                {
                    currentMonth="נובמבר";
                    updateShiftsList(11);
                }
                if(parent.getItemAtPosition(position).toString().equals("דצמבר"))
                {
                    currentMonth="דצמבר";
                    updateShiftsList(12);
                }

            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
    }
    // when choosing month from the spinner , updating the list in the listview by reading the database again
    private void updateShiftsList(int i) {
        SQLiteDatabase db = dbHelper.getReadableDatabase();
        String[] whereArgs = new String[] {
                ""+i
        };
        String[] cols=new String[]{Shift._ID,Shift.DATE,Shift.SHIFT_IN,Shift.SHIFT_OUT,Shift.TOTAL_SHIFTS,Shift.MONTH};

        Cursor cursor=db.query(Shift.TABLE_NAME, cols, Shift.MONTH+"=?", whereArgs, null, null, null);
        textToShare="";
        if (cursor != null)// If Cursordepot is null then do
        // nothing
        {
            if (cursor.moveToFirst()) {


                do {
                    if(cursor.getString(cursor.getColumnIndex(Shift.DATE)).equals(""))
                        break;
                    textToShare+="["+cursor.getString(cursor.getColumnIndex(Shift.DATE))+"] "+
                         " כניסה:"  +cursor.getString(cursor.getColumnIndex(Shift.SHIFT_IN))+
                         " יציאה:"  +cursor.getString(cursor.getColumnIndex(Shift.SHIFT_OUT))+
                         " סך הכל :"  +cursor.getString(cursor.getColumnIndex(Shift.TOTAL_SHIFTS)) +"\n";
                } while (cursor.moveToNext());
            }
            if(!textToShare.equals("")) {
                textToShare += "שותף מ:";
                textToShare += " AutoSalary"+
                        "\nhttps://play.google.com/store/apps/details?id=com.autosalary2.mohamdib.finalprojectmrm";
            }
        }


        sca.changeCursor(cursor);
        listOfShifts.setAdapter(sca);
        Float total=0.0f;
        if (cursor.moveToFirst()) {

            do {
                if(cursor.getString(cursor.getColumnIndex(Shift.DATE)).equals(""))
                    break;
                total+=Float.parseFloat(cursor.getString(cursor.getColumnIndex(Shift.TOTAL_SHIFTS)).toString());


            } while (cursor.moveToNext());
        }

        tvTotal.setTextSize(20);
        tvTotal.setText("המשכורת שלך לחודש הזה: "+calculateSalary(total) + "₪");
        db.close();
    }
    private String getTotalTime() {
        Date date1, date2;
        long difference;
        SimpleDateFormat format = new SimpleDateFormat("HH:mm");
        String total = "";
        try {
            date1 = format.parse(timeIn);
            date2 = format.parse(timeOut);
            difference = date2.getTime() - date1.getTime();
            long diffMinutes = difference / (60 * 1000) % 60;
            long diffHours = difference / (60 * 60 * 1000) % 24;
            total = "" + diffHours + "." + diffMinutes + "\n";
            //       tvRes.setText(tvRes.getText()+"    סך הכל :"+ diffHours+"."+diffMinutes +"\n");
        } catch (Exception e) {

        }
        return total;
    }
    //  calculating the total salary for the current Month
    private double calculateSalary(Float total) {
        SharedPreferences settings=getSharedPreferences("AutoSalaryPrefs",MODE_PRIVATE);
        Float hourly=settings.getFloat("hourly",0.0f);
        float totalInShekels=total*hourly;
        return (double)Math.round(totalInShekels*100.0)/100.0;
    }
    // spinner initial
    private void initSpinner() {
        List<String> list = new ArrayList<String>();
        list.add("ינואר");
        list.add("פברואר");
        list.add("מרץ");
        list.add("אפריל");
        list.add("מאי");
        list.add("יוני");
        list.add("יולי");
        list.add("אוגוסט");
        list.add("ספטמבר");
        list.add("אוקטובר");
        list.add("נובמבר");
        list.add("דצמבר");
        ArrayAdapter<String> dataAdapter = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_item, list);
        dataAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spMonth.setAdapter(dataAdapter);
    }

    // print Toast ..
    private void printToast(String message) {
        Toast.makeText(ShiftLists.this, message, Toast.LENGTH_LONG).show();

    }
    private int getNumOfMonth(String month)
    {
        if(month.equals("ינואר"))
            return 1;
        if(month.equals("פברואר"))
            return 2;
        if(month.equals("מרץ"))
            return 3;
        if(month.equals("אפריל"))
            return 4;
        if(month.equals("מאי"))
            return 5;
        if(month.equals("יוני"))
            return 6;
        if(month.equals("יולי"))
            return 7;
        if(month.equals("אוגוסט"))
            return 8;
        if(month.equals("ספטמבר"))
            return 9;
        if(month.equals("אוקטובר"))
            return 10;
        if(month.equals("נובמבר"))
            return 11;
        if(month.equals("דצמבר"))
            return 12;
        return 0;

    }
    private String getDate() {
        Calendar c = Calendar.getInstance();
        int m = c.get(Calendar.MONTH);
        m++;
        String m1 = "";
        int d = c.get(Calendar.DAY_OF_MONTH);
        String d1 = "";
        if (d < 10)
            d1 = "0" + d;
        else
            d1 = "" + d;
        if (m < 10)
            m1 = "0" + m;
        else
            m1 = "" + m;

        return d + "/" + m;
    }
}






/*   SQLiteDatabase db = dbHelper.getReadableDatabase();
                                Cursor cursor = db.query(Shift.TABLE_NAME, null, null, null, null, null, null);
                                db.delete(Shift.TABLE_NAME,"_id=?",new String[]{""+ItemId});
                                sca=new ShiftsCursorAdapter(ShiftLists.this,cursor);
                                listOfShifts.setAdapter(sca);
                                db.close();
                               */