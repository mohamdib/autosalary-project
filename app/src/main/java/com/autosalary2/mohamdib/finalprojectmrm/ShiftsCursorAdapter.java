package com.autosalary2.mohamdib.finalprojectmrm;

import android.content.Context;
import android.database.Cursor;
import android.graphics.Color;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CursorAdapter;
import android.widget.TextView;

/**
 * Created by mohamdib on 1/26/2017.
 */

public class ShiftsCursorAdapter extends CursorAdapter {
    LayoutInflater inflater;

    public ShiftsCursorAdapter(Context context , Cursor c)
    {
        super(context,c,true);
        inflater=LayoutInflater.from(context);

    }
    @Override
    public View newView(Context context, Cursor cursor, ViewGroup parent) {

        return inflater.inflate(R.layout.shift_view,parent,false);


    }
    // in bindView we add the values from the database into each view in the listView
    @Override
    public void bindView(View view, Context context, Cursor cursor) {
           TextView tvDate = (TextView) view.findViewById(R.id.tvDate);
           TextView tvIn = (TextView) view.findViewById(R.id.tvIn);
           TextView tvOut = (TextView) view.findViewById(R.id.tvOut);
           TextView tvTotal = (TextView) view.findViewById(R.id.tvTotal);
           tvDate.setText("ב:" + cursor.getString(cursor.getColumnIndex(Shift.DATE)).toString());
           tvIn.setText("כנ:" + cursor.getString(cursor.getColumnIndex(Shift.SHIFT_IN)).toString());
           tvOut.setText("יצ:" + cursor.getString(cursor.getColumnIndex(Shift.SHIFT_OUT)).toString());
           tvTotal.setText("סה\"כ:" + cursor.getString(cursor.getColumnIndex(Shift.TOTAL_SHIFTS)).toString() + "שע'");

           // each line with different color
           if (cursor.getPosition() % 2 == 0) {
               view.setBackgroundColor(Color.WHITE);
           }
           if (cursor.getPosition() % 2 == 1)
               view.setBackgroundColor(Color.parseColor("#BAB4B4"));
    }


}
