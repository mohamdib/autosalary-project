package com.autosalary2.mohamdib.finalprojectmrm;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

/**
 * Created by mohamdib on 1/26/2017.
 */

public class ShiftsDbHelper extends SQLiteOpenHelper {
    private static final String SQL_CREATE_TABLE="CREATE TABLE "+Shift.TABLE_NAME+" ("+
            Shift._ID+ " INTEGER PRIMARY KEY,"+
            Shift.DATE + " TEXT,"+
            Shift.SHIFT_IN + " TEXT,"+
            Shift.SHIFT_OUT+ " TEXT,"+
            Shift.TOTAL_SHIFTS+ " TEXT,"+
            Shift.MONTH+ " TEXT"+
            ");";
    private  static final String SQL_DELETE="DROP TABLE IF EXISTS "+Shift.TABLE_NAME;
    public static final int DATABASE_VERSION= 1;
    public static final String DATABASE_NAME="myshifts.db";

    public ShiftsDbHelper(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {

        db.execSQL(SQL_CREATE_TABLE);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        db.execSQL(SQL_DELETE);
        onCreate(db);
    }

}
