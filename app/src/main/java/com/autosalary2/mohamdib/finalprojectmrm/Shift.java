package com.autosalary2.mohamdib.finalprojectmrm;

import android.provider.BaseColumns;

/**
 * Created by mohamdib on 1/26/2017.
 */


// shift table

public class Shift implements BaseColumns {

    public static final String TABLE_NAME="shiftsTable";
    public static final String DATE="date";
    public static final String SHIFT_IN="shiftIn";
    public static final String SHIFT_OUT="shiftOut";
    public static final String TOTAL_SHIFTS="totalShifts";
    public static final String MONTH="month";



}
