package com.autosalary2.mohamdib.finalprojectmrm;

import android.content.Context;
import android.support.v4.view.PagerAdapter;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

/**
 * Created by mohamdib on 11/02/2017.
 */


// adapter class for the ABOUT US activity , can show the team photos and information for each one
public class CustomSwipeAdapter extends PagerAdapter{


    private int[] image_resources = {R.mipmap.mohmd,R.mipmap.roi,R.mipmap.moria};
    private Context ctx;
    private LayoutInflater layoutInflater;
    public CustomSwipeAdapter(Context ctx)
    {
        this.ctx=ctx;
    }

    @Override
    public int getCount() {
        return image_resources.length;

    }

    @Override
    public boolean isViewFromObject(View view, Object object) {
        return (view==(LinearLayout)object);
    }

    @Override
    public Object instantiateItem(ViewGroup container, int position) {
        layoutInflater=(LayoutInflater)ctx.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View item_view=layoutInflater.inflate(R.layout.swipe_layout,container,false);
        ImageView imageView=(ImageView)item_view.findViewById(R.id.imageView);
        TextView tvName=(TextView)item_view.findViewById(R.id.tvName);
        TextView tvDetails=(TextView)item_view.findViewById(R.id.tvDetails);
        imageView.setImageResource(image_resources[position]);
        if(position==0) {
            tvName.setText("Mohammed iBRAHEM ");
            tvDetails.setText("Student For Software Engineering");
        }
        if(position==2) {
            tvName.setText("Moria Tochband ");
            tvDetails.setText("Student For Software Engineering");



        }
        if(position==1) {
            tvName.setText("Roi Cohen ");
            tvDetails.setText("Student For Software Engineering");

        }
        container.addView(item_view);

        return item_view;
    }

    @Override
    public void destroyItem(ViewGroup container, int position, Object object) {
        container.removeView((LinearLayout)object);

    }
}
