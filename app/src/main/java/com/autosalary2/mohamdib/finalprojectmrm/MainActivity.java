package com.autosalary2.mohamdib.finalprojectmrm;

import android.content.ContentValues;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.ActivityInfo;
import android.database.sqlite.SQLiteDatabase;
import android.graphics.Typeface;
import android.net.Uri;
import android.os.CountDownTimer;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

public class MainActivity extends AppCompatActivity {

    // this method called when the activity starts , check if it the first time open the application
    // in order to get the settings from the user just on the first time !! .

    @Override
    protected void onStart() {
        super.onStart();
        SharedPreferences settings=getSharedPreferences("AutoSalaryPrefs",MODE_PRIVATE);
        boolean firstTime=settings.getBoolean("firstTime",true);
        if(firstTime)
        {
            settings.edit().putBoolean("firstTime",false).apply();
            Intent i = new Intent(MainActivity.this, SettingsActivity.class);
            startActivity(i);
        }

    }

    /*  views */
    TextView tvTimer;
    Button btIn, btOut, btSettings, btaboutUs, btList,btShare,btInfo;
    String dataBaseDate, dataBaseIn, dataBaseOut, dataBaseTotal,active; // send shift to the database as string
    Calendar c;
    ShiftsDbHelper dbHelper;
    SQLiteDatabase db;
    String timeIn, timeOut;// string values for calculating the total
    TextView tvDetails,tvDate;

    boolean isclicked;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        // to delete the action bar
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        this.getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        getSupportActionBar().hide();
        setContentView(R.layout.activity_main);
        setRequestedOrientation (ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);



        tvDetails=(TextView)findViewById(R.id.tvDetails);
        tvDate=(TextView)findViewById(R.id.tvDate);
        // setting the current date and show it in the activity
        setDateInTextView();
        // after we have the user ionformation like " name , work location , salary per hour ..
        // get it from the sharedpreferences and show it in the activity
        initDetails();
        // the data that we can save as string into the database "SQLITE"
        dataBaseDate = "";
        dataBaseIn = "";
        dataBaseOut = "";
        dataBaseTotal = "";

        dbHelper = new ShiftsDbHelper(this);
        // temp variables to save the timeiN AND timeOut
        timeIn = "";
        timeOut = "";
        /****    Buttons Initial    ******/
        btShare= (Button) findViewById(R.id.btShare);
        btIn = (Button) findViewById(R.id.btIn);
        btOut = (Button) findViewById(R.id.btOut);
        btSettings = (Button) findViewById(R.id.btSettings);
        btaboutUs = (Button) findViewById(R.id.btAboutUs);
        btList = (Button) findViewById(R.id.btList);
        btInfo= (Button) findViewById(R.id.btInfo);
        btOut.setBackgroundResource(R.drawable.fing_out_off);
        btOut.setLongClickable(false);

        /* buttons on long click  */
        btInfo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("https://youtu.be/HXaeEXRIH3g")));
            }
        });
        btShare.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent sharingIntent = new Intent(android.content.Intent.ACTION_SEND);
                sharingIntent.setType("text/plain");
                sharingIntent.putExtra(android.content.Intent.EXTRA_SUBJECT,"שיתוף אפליקציה Auto Salary");
                sharingIntent.putExtra(android.content.Intent.EXTRA_TEXT,
                        "אני נהניתי מהאפליקציה של שמירת משמרות שלי! בואו תהנו גם אתם ותורידו מ: "+"\n" +
                                "https://play.google.com/store/apps/details?id=com.autosalary2.mohamdib.finalprojectmrm");
                startActivity(Intent.createChooser(sharingIntent, "שתף את המשמרות"));
            }
        });
        btSettings.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                    Intent i = new Intent(MainActivity.this, SettingsActivity.class);
                    startActivity(i);
            }
        });
        btaboutUs.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i=new Intent(MainActivity.this,AboutUsActivity.class);
                startActivity(i);
            }
        });
        // the user should press long click on the sign button.
        btOut.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                printToast("נא ללחוץ לחיצה ארוכה");
            }
        });
        btIn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                printToast("נא ללחוץ לחיצה ארוכה");
            }
        });

        btIn.setOnLongClickListener(new View.OnLongClickListener() {
            @Override
            public boolean onLongClick(View v) {
                isclicked=true;
                c = Calendar.getInstance();
                btIn.setBackgroundResource(R.drawable.finger_in_off);
                btOut.setBackgroundResource(R.drawable.fing_out);
                btIn.setLongClickable(false);
                btOut.setLongClickable(true);
                timeIn = c.get(Calendar.HOUR_OF_DAY) + ":" + c.get(Calendar.MINUTE);
                dataBaseDate = getDate();
                dataBaseIn = getTime();
                Toast.makeText(MainActivity.this, "חתמת כניסה ב:" + DateFormat.getDateTimeInstance().format(new Date()),
                        Toast.LENGTH_LONG).show();
           //     disableAllButtons();
                return true;
            }
        });
        btOut.setOnLongClickListener(new View.OnLongClickListener() {
            @Override
            public boolean onLongClick(View v) {
                isclicked=false;

                // calendar to get date & time
                c = Calendar.getInstance();
                timeOut = c.get(Calendar.HOUR_OF_DAY) + ":" + c.get(Calendar.MINUTE);
                btOut.setBackgroundResource(R.drawable.fing_out_off);
                btIn.setBackgroundResource(R.drawable.fing_in);
                btIn.setLongClickable(true);
                btOut.setLongClickable(false);
                dataBaseOut = getTime();
                dataBaseTotal = getTotalTime();
                printToast("חתמת יציאה ב:" + DateFormat.getDateTimeInstance().format(new Date()));

                // when this button has pressed , send all the data to the database

                sendToDataBase(dataBaseDate, dataBaseIn, dataBaseOut, dataBaseTotal);


                return true;
            }
        });
        btList.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(MainActivity.this, ShiftLists.class);
                startActivity(i);
            }
        });
        tvTimer = (TextView) findViewById(R.id.tvTimer);
        tvTimer.setTypeface(null, Typeface.BOLD);
        startTimer();


    }




    private void setDateInTextView() {
        c = Calendar.getInstance();
        int month=c.get(Calendar.MONTH);
        month+=1;
       tvDate.setText("תאריך: "+c.get(Calendar.DAY_OF_MONTH)+"/"+month+"/"+c.get(Calendar.YEAR));
    }

    // calculate the difference between begin and end the work.
    private String getTotalTime() {
        Date date1, date2;
        long difference;
        SimpleDateFormat format = new SimpleDateFormat("HH:mm");
        String total = "";
        try {
            date1 = format.parse(timeIn);
            date2 = format.parse(timeOut);
            difference = date2.getTime() - date1.getTime();
            long diffMinutes = difference / (60 * 1000) % 60;
            long diffHours = difference / (60 * 60 * 1000) % 24;
            total = "" + diffHours + "." + diffMinutes + "\n";
            //       tvRes.setText(tvRes.getText()+"    סך הכל :"+ diffHours+"."+diffMinutes +"\n");
        } catch (Exception e) {

        }
        return total;
    }

    // print Toast ..
    private void printToast(String message) {
        Toast.makeText(MainActivity.this, message, Toast.LENGTH_LONG).show();

    }




    private void initDetails() {
        SharedPreferences settings=getSharedPreferences("AutoSalaryPrefs",MODE_PRIVATE);
        String name=settings.getString("userName","משתמש יקר");
        String loc=settings.getString("location","מיקום העסקתך");
        Float hourly=settings.getFloat("hourly",0.0f);
        tvDetails.setText("שלום רב "+name+"\n"
        +"איזור העבודה הוא :"+loc+"\n"
        +"תעריף לשעה :" + hourly+"\n"
         +"(כנס להגדרות כדי לערוך את הפרטים שלך)");
    }
    // get the current time as string
    private String getTime() {
        int h = c.get(Calendar.HOUR_OF_DAY);
        int m = c.get(Calendar.MINUTE);
        int s = c.get(Calendar.SECOND);
        String h1, m1, s1;
        if (h < 10)
            h1 = "0" + h;
        else
            h1 = "" + h;
        if (m < 10)
            m1 = "0" + m;
        else
            m1 = "" + m;
        if (s < 10)
            s1 = "0" + s;
        else
            s1 = "" + s;
        return h1 + ":" + m1;
    }
// get the current date as string

    private String getDate() {
        Calendar c = Calendar.getInstance();
        int m = c.get(Calendar.MONTH);
        m++;
        String m1 = "";
        int d = c.get(Calendar.DAY_OF_MONTH);
        String d1 = "";
        if (d < 10)
            d1 = "0" + d;
        else
            d1 = "" + d;
        if (m < 10)
            m1 = "0" + m;
        else
            m1 = "" + m;

        return d + "/" + m;
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
    }
    // open database to write, and add the values ..

    private void sendToDataBase(String dataBaseDate, String dataBaseIn, String dataBaseOut, String dataBaseTotal) {
        Calendar c = Calendar.getInstance();
        int m = c.get(Calendar.MONTH);
        m++;
        db = dbHelper.getWritableDatabase();
        ContentValues values = new ContentValues();
        values.put(Shift.DATE, dataBaseDate);
        values.put(Shift.SHIFT_IN, dataBaseIn);
        values.put(Shift.SHIFT_OUT, dataBaseOut);
        values.put(Shift.TOTAL_SHIFTS, dataBaseTotal);
        values.put(Shift.MONTH,""+m);
        long id;
        id = db.insert(Shift.TABLE_NAME, null, values);
        db.close();


    }

    // Timer Method from the current Time
    private void startTimer() {

        CountDownTimer newtimer = new CountDownTimer(1000000000, 1000) {
        // timer to be shown on the main activity , so this function will start count and add 0 beside al the digits
        // that less than 10
            public void onTick(long millisUntilFinished) {
                Calendar c = Calendar.getInstance();
                int h = c.get(Calendar.HOUR_OF_DAY);
                int m = c.get(Calendar.MINUTE);
                int s = c.get(Calendar.SECOND);
                String h1, m1, s1;
                if (h < 10)
                    h1 = "0" + h;
                else
                    h1 = "" + h;
                if (m < 10)
                    m1 = "0" + m;
                else
                    m1 = "" + m;
                if (s < 10)
                    s1 = "0" + s;
                else
                    s1 = "" + s;
                tvTimer.setText("שעה:" + h1 + ":" + m1 + ":" + s1);

            }

            public void onFinish() {

            }
        };
        newtimer.start();
    }





/*                                  saving the state of the activity                                      */

    @Override
    protected void onPause() {
        super.onPause();
        SharedPreferences.Editor editor=getSharedPreferences("onPauseMethod", MODE_PRIVATE).edit();
        if(isclicked)
        {
            editor.putBoolean("clicked",true);
            editor.putString("timeIn",timeIn);
            editor.putString("dataBaseDate",dataBaseDate);
            editor.putString("dataBaseIn",dataBaseIn);
            editor.putString("dataBaseOut",dataBaseOut);
            editor.putString("dataBaseTotal",dataBaseTotal);
            Log.i("HIHIHI","yes im here 1 ");
        }
        else
            editor.putBoolean("clicked",false);

        editor.apply();



    }
    @Override
    protected void onResume() {
        super.onResume();
        SharedPreferences settings=getSharedPreferences("onPauseMethod",MODE_PRIVATE);
        boolean clicked = settings.getBoolean("clicked",false);
        if(clicked)
        {
            timeIn= settings.getString("timeIn",timeIn);
            dataBaseDate=settings.getString("dataBaseDate","");
            dataBaseIn=settings.getString("dataBaseIn","");
            dataBaseOut=settings.getString("dataBaseOut","");
            dataBaseTotal=settings.getString("dataBaseTotal","");
            c = Calendar.getInstance();
            btIn.setBackgroundResource(R.drawable.finger_in_off);
            btOut.setBackgroundResource(R.drawable.fing_out);
            btIn.setLongClickable(false);
            btOut.setLongClickable(true);
            Log.i("HIHIHI","yes im here 2 ");
        }
    }

    @Override
    protected void onStop() {
        super.onStop();
        SharedPreferences.Editor editor=getSharedPreferences("onPauseMethod", MODE_PRIVATE).edit();
        if(isclicked)
        {
            editor.putBoolean("clicked",true);
            editor.putString("timeIn",timeIn);
            editor.putString("dataBaseDate",dataBaseDate);
            editor.putString("dataBaseIn",dataBaseIn);
            editor.putString("dataBaseOut",dataBaseOut);
            editor.putString("dataBaseTotal",dataBaseTotal);
            Log.i("HIHIHI","yes im here 1 ");
        }
        else
            editor.putBoolean("clicked",false);

        editor.apply();

    }


    // we want to save the state of the singature ! if we have singed in , so we want to save it for the next time
    //in order to not allow sign in again

    @Override
    protected void onRestart() {
        super.onRestart();
        SharedPreferences settings=getSharedPreferences("onPauseMethod",MODE_PRIVATE);
        boolean clicked = settings.getBoolean("clicked",false);
        if(clicked)
        {
            timeIn= settings.getString("timeIn",timeIn);
            dataBaseDate=settings.getString("dataBaseDate","");
            dataBaseIn=settings.getString("dataBaseIn","");
            dataBaseOut=settings.getString("dataBaseOut","");
            dataBaseTotal=settings.getString("dataBaseTotal","");
            c = Calendar.getInstance();
            btIn.setBackgroundResource(R.drawable.finger_in_off);
            btOut.setBackgroundResource(R.drawable.fing_out);
            btIn.setLongClickable(false);
            btOut.setLongClickable(true);
            Log.i("HIHIHI","yes im here 2 ");
        }
    }

}










