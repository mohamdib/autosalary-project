package com.autosalary2.mohamdib.finalprojectmrm;

import android.Manifest;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.location.Criteria;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Build;
import android.os.Bundle;
import android.os.IBinder;
import android.support.v4.app.NotificationCompat;


public class LocationWorkService extends Service {

    // location variable to compare with the current location
    private Location targetLocation;

    private LocationManager locationManager;
    Context context;
    private float distance;
    // Geocoder



    // permissions
    static final int MY_PERMISSIONS_REQUEST_LOCATION=1;
    static final int MY_PERMISSIONS_COARSE_LOCATION=1;

    // max result of the addresses in GeoCoder
    static final int MAX_RESULT=5;
    // location listener
    private LocationListener locationListener = new LocationListener() {


        // when the location changed, compare the current location with the target location , by distance difference !
        @Override
        public void onLocationChanged(Location location) {

            if(targetLocation.distanceTo(location)<=distance)
            {
                // notification when the user is near the work location , so he can press the notification and
                // enter the application to sign in

                Context context=getApplicationContext();
                Intent intent = new Intent(LocationWorkService.this,MainActivity.class);
                intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                PendingIntent pendingIntent = PendingIntent.getActivity(LocationWorkService.this, 0, intent, 0);
                SharedPreferences settings=getSharedPreferences("AutoSalaryPrefs",MODE_PRIVATE);
                String loc=settings.getString("location","מיקום העסקתך");
                NotificationCompat.Builder mBuilder =
                        new NotificationCompat.Builder(LocationWorkService.this)
                                .setSmallIcon(R.mipmap.ic_launcher)
                                .setContentTitle("אתה כרגע באיזור " + loc)
                                .setContentIntent(pendingIntent)
                                .setContentText("לחץ פה להחתמת כניסה/יציאה ");

                NotificationManager mNotificationManager =
                        (NotificationManager) getSystemService(
                                Context.NOTIFICATION_SERVICE);
                mNotificationManager.notify(1, mBuilder.build());

            }
        }

        @Override
        public void onStatusChanged(String provider, int status, Bundle extras) {
        }

        @Override
        public void onProviderEnabled(String provider) {
        }

        @Override
        public void onProviderDisabled(String provider) {
        }
    };


    // constructor
    public LocationWorkService()
    {

    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {

            if (checkSelfPermission(Manifest.permission.ACCESS_COARSE_LOCATION) !=
                    PackageManager.PERMISSION_GRANTED) {
                return;

            }
        }
        locationManager.removeUpdates(locationListener);
        locationManager = null;


    }

    @Override
    public IBinder onBind(Intent intent) {
        // TODO: Return the communication channel to the service.
        throw new UnsupportedOperationException("Not yet implemented");
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        // init the locationManager
        locationManager = (LocationManager) getSystemService(LOCATION_SERVICE);
        // init the target location
        targetLocation =new Location("");
        //get lon and lat from the intent , and convert from string to float
        String lat= intent.getExtras().getString("lat");
        String lon= intent.getExtras().getString("lon");
        // set the lat and lon to the "target location" variable
        targetLocation.setLatitude((float)Float.valueOf(lat));
        targetLocation.setLongitude((float)Float.valueOf(lon));
        // get the distance from the intent , convert from string to float
        String distanceString=intent.getExtras().getString("distance");
        distance=(float)Float.valueOf(distanceString);

        // check permission
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {

            if (checkSelfPermission(Manifest.permission.ACCESS_FINE_LOCATION) !=
                    PackageManager.PERMISSION_GRANTED)
            {
                return START_NOT_STICKY;

            }
            if( checkSelfPermission(Manifest.permission.ACCESS_COARSE_LOCATION) !=
                    PackageManager.PERMISSION_GRANTED) {
                return START_NOT_STICKY;

            }

        }

        // start the location listener with 1 sec or 1 metter
        //   locationManager.removeUpdates(locationListener);

        Criteria criteria=new Criteria();
        criteria.setAccuracy(Criteria.ACCURACY_MEDIUM);
        criteria.setAltitudeRequired(true);
        criteria.setPowerRequirement(Criteria.POWER_HIGH);
        criteria.setCostAllowed(true);
        String best=locationManager.getBestProvider(criteria, false);
        locationManager.requestLocationUpdates(best, 1, 1,locationListener);

        return START_STICKY;
    }


}