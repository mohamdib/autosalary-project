package com.autosalary2.mohamdib.finalprojectmrm;

import android.Manifest;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.ActivityInfo;
import android.location.Address;
import android.location.Geocoder;
import android.os.AsyncTask;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.Toast;

import java.io.IOException;
import java.util.List;

public class SettingsActivity extends AppCompatActivity {
    SharedPreferences.Editor editor;
    Button btBack,btName,btLocation,btSalary,btService;
    EditText edName,edLocation,edHourly,edDis;
    CheckBox cbLocation;


    /* LOCATION VARIABLES */
    // permissions
    static final int MY_PERMISSIONS_REQUEST_LOCATION=1;
    static final int MY_PERMISSIONS_COARSE_LOCATION=1;
    Geocoder geocoder;
    // alertDialog
    AlertDialog.Builder addressesDialog;
    // distance value from the editText
    String Dist;
    // lan and lot that will set to the location variable
    float lat,lon;
    // list of addresses in case we have more than one address from the GeoCoder
    List<Address> lstAdresses;
    static final int MAX_RESULT=5;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        // remove action bar
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        this.getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        getSupportActionBar().hide();
        setContentView(R.layout.activity_settings);

        setRequestedOrientation (ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);

        // initial views
        cbLocation=(CheckBox)findViewById(R.id.cbLocation);
        btName=(Button)findViewById(R.id.btSaveName);
        btLocation=(Button)findViewById(R.id.btSearchLoc);
        btSalary=(Button)findViewById(R.id.btSaveHourly);
        btBack=(Button)findViewById(R.id.btHome);
        edName=(EditText)findViewById(R.id.edName);
        edHourly=(EditText)findViewById(R.id.edHourly);
        edLocation=(EditText)findViewById(R.id.edLocation);
        btService=(Button)findViewById(R.id.btService);
        edDis=(EditText)findViewById(R.id.edDis);

        Dist=edDis.getText().toString();
        askPermessions();
        setCheck(cbLocation.isChecked());

        //  update the check box according to the isCheckedBoxChecked value from the sharedPreferences

        SharedPreferences settings=getSharedPreferences("AutoSalaryPrefs",MODE_PRIVATE);
        boolean checkBoxIsCheck=settings.getBoolean("isCheckBoxChecked",false);
        setCheckBox(checkBoxIsCheck);
        updateEditTexts();
        btName.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                editor = getSharedPreferences("AutoSalaryPrefs", MODE_PRIVATE).edit();
                editor.putString("userName",edName.getText().toString());
                editor.apply();
            }
        });

        // using AsyncTask in order to choose location from the location Dialog , so the thread will not work in the Ui thread
        btLocation.setOnClickListener(new View.OnClickListener() {
              @Override
              public void onClick(View v) {
                  new    MyAsyncTask(SettingsActivity.this).execute();
              }
          }
        );
        // this button is
        btService.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(SettingsActivity.this, LocationWorkService.class);
                intent.putExtra("lat",""+lat);
                intent.putExtra("lon",""+lon);
                intent.putExtra("distance", edDis.getText().toString());

                //request permission
                askPermessions();
                // start the service
                editor = getSharedPreferences("AutoSalaryPrefs", MODE_PRIVATE).edit();
                editor.putString("distance",edDis.getText().toString());
                editor.apply();
                startService(intent);


            }
        });
        btSalary.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                editor = getSharedPreferences("AutoSalaryPrefs", MODE_PRIVATE).edit();
                float f = Float.parseFloat(edHourly.getText().toString());
                editor.putFloat("hourly",f);
                editor.apply();

            }
        });
        btBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i=new Intent(SettingsActivity.this,MainActivity.class);
                startActivity(i);
            }
        });
        cbLocation.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {

          @Override
          public void onCheckedChanged(CompoundButton buttonView,boolean isChecked) {
              setCheck(isChecked);
          }
      }
        );
    }

    private void setCheck(boolean isChecked) {
        if(isChecked)
        {
            editor = getSharedPreferences("AutoSalaryPrefs", MODE_PRIVATE).edit();
            editor.putBoolean("isCheckBoxChecked",true);
            editor.apply();
            btLocation.setClickable(true);
            btLocation.setEnabled(true);
            edLocation.setEnabled(true);
            btService.setClickable(true);
            edDis.setEnabled(true);
            btService.setEnabled(true);

        }
        else {
            editor = getSharedPreferences("AutoSalaryPrefs", MODE_PRIVATE).edit();
            editor.putBoolean("isCheckBoxChecked",false);
            editor.apply();
            btLocation.setClickable(false);
            btLocation.setEnabled(false);
            edLocation.setEnabled(false);
            btService.setClickable(false);
            btService.setEnabled(false);
            edDis.setEnabled(false);
            Intent intent = new Intent(SettingsActivity.this, LocationWorkService.class);
            stopService(intent);


        }
    }

    private void updateEditTexts() {

        SharedPreferences settings=getSharedPreferences("AutoSalaryPrefs",MODE_PRIVATE);
        String loc=settings.getString("location","");
        if(loc.equals(""))
            edLocation.setText("");
        else
            edLocation.setText(loc);
        String name=settings.getString("userName","");
        if(name.equals(""))
            edName.setText("");
        else
            edName.setText(name);
        String dis=settings.getString("distance","");
        if(dis.equals(""))
            edDis.setText("");
        else
            edDis.setText(dis);
        String hourly=""+settings.getFloat("hourly",0.0f);
        if(hourly.equals(""))
            edHourly.setText("");
        else
            edHourly.setText(hourly);


    }

    private void setCheckBox(boolean check) {
        editor = getSharedPreferences("AutoSalaryPrefs", MODE_PRIVATE).edit();
        editor.putBoolean("isCheckBoxChecked",check);
        editor.apply();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        Intent intent = new Intent(SettingsActivity.this, LocationWorkService.class);
        stopService(intent);
    }

    // method that ask permissions
    private void askPermessions()
    {
        ActivityCompat.requestPermissions(SettingsActivity.this
                ,new  String[]{Manifest.permission.ACCESS_FINE_LOCATION},
                MY_PERMISSIONS_REQUEST_LOCATION);
        ActivityCompat.requestPermissions(SettingsActivity.this
                ,new  String[]{Manifest.permission.ACCESS_COARSE_LOCATION},
                MY_PERMISSIONS_COARSE_LOCATION);

    }


    private class MyAsyncTask extends AsyncTask<Object, Object, List<Address>>
    {
        private ProgressDialog dialog;
        private Context context;
        public MyAsyncTask(Context context)
        {
            this.context=context;
        }

        // method that return list of the addresses
        private List<Address> searchAddresses()
        {
            geocoder = new Geocoder(SettingsActivity.this);
            try
            {
                return  geocoder.getFromLocationName(edLocation.getText().toString(), MAX_RESULT);
            }
            catch (IOException e)
            {
            }
            return null;
        }

        // on the background , go and get the addresses , and then cancle the loading dialog
        @Override
        protected List<Address> doInBackground(Object... params)
        {
            return searchAddresses();
        }

        protected void onPreExecute()
        {
            // loading dialog is waiting until the search is finished!
            this.dialog = new ProgressDialog(context);
            this.dialog.setMessage("Loading...");
            this.dialog.setCancelable(true);
            this.dialog.setOnCancelListener(new DialogInterface.OnCancelListener(){
                @Override
                public void onCancel(DialogInterface dialog)
                {
                    // cancel AsyncTask
                    cancel(false);
                }
            });

            this.dialog.show();

        }
        protected void onPostExecute(List<Address> result)
        {
            lstAdresses = result;
            if (lstAdresses ==null)
            {
                return;

            }
            if(lstAdresses.size()>1)
            {
                // if the list size is more than 1 result , init a array from type charSequence in order to put them in the dialog
                // and then if the user choosed any item , set the text into the textView of the target !

                addressesDialog = new AlertDialog.Builder(SettingsActivity.this);
                final CharSequence items[]=new CharSequence[lstAdresses.size()];
                for(int i = 0; i< lstAdresses.size(); i++)
                {
                    Address location= lstAdresses.get(i);

                    items[i]=location.getAddressLine(0)+" "+location.getAddressLine(1);
                }
                addressesDialog.setSingleChoiceItems(items, 0, new DialogInterface.OnClickListener() {

                    @Override
                    public void onClick(DialogInterface d, int n) {
                        printToast(""+items[n]);
                        editor = getSharedPreferences("AutoSalaryPrefs", MODE_PRIVATE).edit();
                        editor.putString("location",""+items[n]);
                        editor.apply();
                    }

                });

                addressesDialog.setNegativeButton("SELECT", null);
                addressesDialog.setTitle("Select specific lstAdresses:");
                addressesDialog.show();
            }
            else if (lstAdresses.size()==0){

                printToast("כתובת אינה חוקית!");

            }

            else{

                Address location= lstAdresses.get(0);
                // if their is one Result so set it to the textview
                printToast(""+location.getAddressLine(0)+" "+location.getAddressLine(1));
                editor = getSharedPreferences("AutoSalaryPrefs", MODE_PRIVATE).edit();
                editor.putString("location",""+location.getAddressLine(0)+" "+location.getAddressLine(1));
                editor.apply();
                printToast("Longtude:"+ lstAdresses.get(0).getLongitude()+", Latitude:"+ lstAdresses.get(0).getLatitude());
                lat=(float) lstAdresses.get(0).getLatitude();
                lon=(float) lstAdresses.get(0).getLongitude();

            }
            dialog.dismiss();
        }


    }
    private void printToast(String message) {
        Toast.makeText(SettingsActivity.this, message, Toast.LENGTH_LONG).show();

    }


}
